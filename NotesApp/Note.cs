﻿using System.Text.RegularExpressions;
using System.Xml;

namespace NotesApp
{
    /// <summary>
    /// Модель данных заметки.
    /// Метаданные и контент.
    /// </summary>
    internal class Note
    {
        private static MetadataSerializer s_serializer;
        private static Regex s_regex;

        static Note()
        {
            s_serializer = new MetadataSerializer();
            s_regex = new Regex("^<!--(.*)-->(.*)", RegexOptions.Singleline);
        }

        /// <summary>
        /// Возвращает или устанавливает метаданные заметки.
        /// </summary>
        public NoteMetadata Metadata { get; set; }

        /// <summary>
        /// Возвращает или устанавливает контент заметки.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.Note"/>.
        /// </summary>
        /// <param name="rawData">Содержимое файла заметки.</param>
        /// <remarks>
        /// Содержимое файла будет разобрано на метаданные и контент.
        /// </remarks>
        public Note(string rawData)
        {
            var match = s_regex.Match(rawData);

            if (match.Success)
            {
                var metadata = match.Groups[1].Value;

                try
                {
                    s_serializer.Load(metadata);
                    Metadata = s_serializer.Data;
                }
                catch (XmlException)
                {
                    Metadata = new NoteMetadata();
                    Metadata.Title = metadata;
                }

                Content = match.Groups[2].Value;
            }
            else
            {
                Metadata = new NoteMetadata();
                Content = rawData;
            }
        }

        /// <inheritdoc />
        public override string ToString()
        {
            s_serializer.Data = Metadata;
            var metadata = s_serializer.Save();
            var metadataElement = $"<!--{metadata}-->";

            return metadataElement + Content;
        }
    }
}
