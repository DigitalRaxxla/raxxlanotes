﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace NotesApp
{
    /// <summary>
    /// Активити перемещения заметки в другую папку (выбор папки).
    /// </summary>
    [Activity(Label = "Move")]
    public class NoteMoveActivity : Activity
    {
        private string _note;
        private AndroidDiskManager _diskManager;
		private NoteFolder _defaultFolder;
        private ListView _listView;
        private Button _buttonOk;
        private Button _buttonCancell;
        private FoldersAdapter _dataAdapter;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.NoteMove);

            ActionBar.SetTitle(Resource.String.destinationFolder);

            var baseDirName = Resources.GetString(Resource.String.defaultFolder);
            var baseDir = GetDir(baseDirName, FileCreationMode.Private);
            
			_diskManager = AndroidDiskManager.GetDefaultInstance(this);
			_defaultFolder = _diskManager.CurrentFolder;

			if (!_diskManager.IsInBaseAppFolder)
			{
				ActionBar.SetDisplayHomeAsUpEnabled(true);
			}

            _dataAdapter = new FoldersAdapter(this, _diskManager);
            _dataAdapter.ListItemClicked += ListItemClickedHandler;

            _listView = FindViewById<ListView>(Resource.Id.listView);
            _listView.Adapter = _dataAdapter;

            _buttonOk = FindViewById<Button>(Resource.Id.moveNoteButton);
            _buttonOk.Click += ButtonOkClickedHandler;

            _buttonCancell = FindViewById<Button>(Resource.Id.cancellButton);
            _buttonCancell.Click += ButtonCancellClickedHandler;

            _note = Intent.GetStringExtra("note");
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            _buttonOk.Click -= ButtonOkClickedHandler;
            _buttonCancell.Click -= ButtonCancellClickedHandler;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                _diskManager.ChangeFolderUp();
                ActionBar.Subtitle =
                    Path.GetDirectoryName(ActionBar.Subtitle);

				if (_diskManager.IsInBaseAppFolder)
                {
                    ActionBar.SetDisplayHomeAsUpEnabled(false);
                    ActionBar.Subtitle = string.Empty;
                }
            }

            return base.OnOptionsItemSelected(item);
        }

        public void ButtonOkClickedHandler(object sender, EventArgs e)
        {
            var intent = new Intent();
            intent.PutExtra("note", _note);
            intent.PutExtra("folder", _diskManager.CurrentFolder.FullPath);

			_diskManager.ChangeFolder(_defaultFolder);

            SetResult(Result.Ok, intent);
            Finish();
        }

        public void ButtonCancellClickedHandler(object sender, EventArgs e)
        {
			_diskManager.ChangeFolder(_defaultFolder);
            SetResult(Result.Canceled);
            Finish();
        }

        private void ListItemClickedHandler(object sender, EventArgs<IListViewItem> e)
        {
            if (e.Data is NoteFolder folder)
            {
                _diskManager.ChangeFolder(folder);
                ActionBar.SetDisplayHomeAsUpEnabled(true);
                ActionBar.Subtitle = Path.Combine(
                    ActionBar.Subtitle ?? "/",
                    folder.Name);
            }
        }
    }
}
