﻿using System;
using Android.App;
using Android.Views;
using Android.Widget;

namespace NotesApp
{
    /// <summary>
    /// Адаптер данных для списка папок на экране выбора папки при перемещении заметки.
    /// Возвращает только папки. Элементы списка без контекстного меню.
    /// </summary>
    internal class FoldersAdapter : BaseAdapter
    {
        private readonly Activity _activity;
        private readonly AndroidDiskManager _diskManager;
        private readonly LayoutInflater _layoutInflater;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.FoldersAdapter"/>.
        /// </summary>
        /// <param name="activity">Активити, содержащий список.</param>
        /// <param name="diskManager">Дисковый менеджер заметок.</param>
        public FoldersAdapter(
            Activity activity,
            AndroidDiskManager diskManager)
        {
            _activity = activity;
            _diskManager = diskManager;
            _layoutInflater = activity.LayoutInflater;

            _diskManager.ContentUpdated += (sender, e) => Update();
        }

        /// <summary>
        /// Возникает, когда пользователь тапает по элементу списка.
        /// </summary>
        public event EventHandler<EventArgs<IListViewItem>> ListItemClicked;

        /// <inheritdoc />
        public override int Count => _diskManager.Folders.Length;

        public void Update()
        {
            _activity.RunOnUiThread(() => NotifyDataSetChanged());
        }

        /// <inheritdoc />
        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        /// <inheritdoc />
        public override long GetItemId(int position)
        {
            return position;
        }

        /// <inheritdoc />
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var listItemView = _layoutInflater.Inflate(Resource.Layout.ListItemNoMenu, parent, false);

            var title = listItemView.FindViewById<TextView>(Resource.Id.title);
            var item = _diskManager.Folders[position];
            title.SetText(item.Name, TextView.BufferType.Normal);

            var date = listItemView.FindViewById<TextView>(Resource.Id.subTitle);
            date.SetText(
                item.CreationTime.ToShortDateString(),
                TextView.BufferType.Normal);

            var image = listItemView.FindViewById<ImageView>(Resource.Id.icon);
            image.SetImageResource(Resource.Drawable.ic_folder_white_24dp);

            _activity.RegisterForContextMenu(listItemView);
            listItemView.Click += (s, e) => ListItemClickHandler(item);

            return listItemView;
        }

        protected virtual void ListItemClickHandler(NoteFolder item)
        {
            ListItemClicked?.Invoke(this, new EventArgs<IListViewItem>(item));
        }
    }
}
