﻿using System;

namespace NotesApp
{
    /// <summary>
    /// Обобщенный класс аргумента события.
    /// </summary>
    internal class EventArgs<T> : EventArgs
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.EventArgs`1"/>.
        /// </summary>
        /// <param name="data">Данные аргумента.</param>
        public EventArgs(T data)
        {
            Data = data;
        }

        /// <summary>
        /// Возвращает данные аргумента события.
        /// </summary>
        public T Data { get; }
    }
}
