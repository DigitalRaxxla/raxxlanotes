﻿using System;
using System.IO;

namespace NotesApp
{
    /// <summary>
    /// Папка с заметками.
    /// </summary>
    public class NoteFolder : IListViewItem
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.NoteFolder"/>.
        /// </summary>
        /// <param name="info">DirectoryInfo директории.</param>
        public NoteFolder(DirectoryInfo info)
        {
            FullPath = info.FullName;
            Info = info;
            Name = info.Name;
            CreationTime = info.CreationTime;
        }

        /// <summary>
        /// Возвращает полное имя папки.
        /// </summary>
        public string FullPath { get; }

        /// <summary>
        /// Возвращает имя папки.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Возвращает DirectoryInfo папки.
        /// </summary>
        public DirectoryInfo Info { get; }

        /// <summary>
        /// Возвращает время создании папки.
        /// </summary>
        public DateTime CreationTime { get; }
    }
}
