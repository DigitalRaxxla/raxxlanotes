﻿using System.IO;
using System.Linq;
using System.Reflection;

namespace NotesApp
{
    /// <summary>
    /// Вспомогательный класс для работы с embedded ресурсами сборки.
    /// </summary>
    static class EmbeddedResource
    {
        private static Assembly s_assembly;
        private static string[] s_resources;

        static EmbeddedResource()
        {
            s_assembly = Assembly.GetExecutingAssembly();
            s_resources = s_assembly.GetManifestResourceNames();
        }

        /// <summary>
        /// Возвращает содержимое ресурса в виде строки.
        /// </summary>
        /// <returns>Прочитанный ресурс.</returns>
        /// <param name="resName">Имя файла embedded ресурса сборки.</param>
        public static string GetString(string resName)
        {
            var resourceName = s_resources.FirstOrDefault(name => name.EndsWith(resName));
            if (resourceName == null)
            {
                return null;
            }

            using (var stream = s_assembly.GetManifestResourceStream(resourceName))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
