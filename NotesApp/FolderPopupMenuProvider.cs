﻿using Android.App;
using Android.Widget;
using Android.Views;
using System;

namespace NotesApp
{
    /// <summary>
    /// Провайдер всплывающих (контекстных) меню для папок в "проводнике" заметок.
    /// </summary>
    internal class FolderPopupMenuProvider
    {
        private readonly Activity _activity;
        private readonly AndroidDiskManager _diskManager;

        private RenameFolderDialog _renameFolderDialog;
        private EventHandler<PopupMenu.MenuItemClickEventArgs> _menuItemClickHandlerWrapper;
        private NoteFolder _folderOld;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.FolderPopupMenuProvider"/>.
        /// </summary>
        /// <param name="activity">Активити, который будет отображать меню.</param>
        /// <param name="diskManager">Дисковый менеджер заметок.</param>
        public FolderPopupMenuProvider(Activity activity, AndroidDiskManager diskManager)
        {
            _activity = activity;
            _diskManager = diskManager;

            _renameFolderDialog = new RenameFolderDialog();
            _renameFolderDialog.NameSelected += RenameFolderHandler;
        }

        /// <summary>
        /// Отображает контекстное меню для папки.
        /// </summary>
        /// <param name="view">View папки, для которого отображается меню.</param>
        /// <param name="folder">Папка, для которой отображается меню.</param>
        public void ShowFolderMenu(View view, NoteFolder folder)
        {
            var menu = new PopupMenu(_activity, view);
            _menuItemClickHandlerWrapper = (sender, e) => MenuItemClickHandler(e, folder);
            menu.MenuItemClick += _menuItemClickHandlerWrapper;
            menu.DismissEvent += DismissEventHandler;
            menu.Inflate(Resource.Menu.FolderPopupMenu);
            menu.Show();
        }

        private void MenuItemClickHandler(PopupMenu.MenuItemClickEventArgs e, NoteFolder folder)
        {
            switch (e.Item.ItemId)
            {
                case Resource.Id.foldersMenuItemRename:
                    _folderOld = folder;
                    _renameFolderDialog.Show(_activity.FragmentManager, "");
                    break;

                case Resource.Id.foldersMenuItemDelete:
                    var confirm = new ConfirmationDialogBase(
                        Resource.String.deleteConfirmationTitle,
                        Resource.String.folderDeleteConfirmationText);

                    confirm.Confirmed += (sender, ea) =>
                    {
                        if (!_diskManager.DeleteFolder(folder))
                        {
                            Toast.MakeText(
                                _activity,
                                Resource.String.deleteFolderError,
                                ToastLength.Long)
                                 .Show();
                            return;
                        }

                        var msg = _activity.Resources.GetString(Resource.String.folderDeletedText);
                        Toast.MakeText(
                            _activity,
                            string.Format(msg, folder.Name),
                            ToastLength.Long)
                            .Show();
                    };

                    confirm.Show(_activity.FragmentManager, "");
                    break;
            }
        }

        private void DismissEventHandler(object sender, PopupMenu.DismissEventArgs e)
        {
            e.Menu.DismissEvent -= DismissEventHandler;
            e.Menu.MenuItemClick -= _menuItemClickHandlerWrapper;
        }

        private void RenameFolderHandler(object sender, EventArgs e)
        {
            var folderNewName = _renameFolderDialog.Name;
            if (!_diskManager.RenameFolder(_folderOld, folderNewName))
            {
                Toast.MakeText(
                    _activity,
                    Resource.String.renameFolderError,
                    ToastLength.Long)
                        .Show();
                return;

            }

            var msg = _activity.Resources.GetString(Resource.String.done);
            Toast.MakeText(_activity, msg, ToastLength.Long).Show();
        }
    }
}
