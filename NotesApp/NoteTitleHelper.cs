﻿using System;
using System.Linq;

namespace NotesApp
{
    /// <summary>
    /// Вспомогательный класс для работы с заголовками заметок.
    /// </summary>
    internal static class NoteTitleHelper
    {
        private const int TitleMaxLength = 32;

        /// <summary>
        /// Возвращает или устанавливает ресурсы приложения.
        /// </summary>
        /// <remarks>
        /// Это свойство должно быть установлено до начала работы с методами класса.
        /// </remarks>
        public static Android.Content.Res.Resources Resources { get; set; }

        /// <summary>
        /// Генерирует дефолтный заголовок заметки из текущих даты и времени.
        /// </summary>
        /// <returns>Название заметки.</returns>
        public static string CreateDefault()
        {
            var now = DateTime.Now;
            var note = Resources.GetString(Resource.String.note);
            return $"{now.ToShortDateString()} {now.ToShortTimeString()} {note}";
        }

        /// <summary>
        /// Извлекает название из первой строки плейн-текстовой заметки.
        /// </summary>
        /// <returns>Название заметки.</returns>
        /// <param name="content">Содержимое плейн-текстовой заметки.</param>
        public static string FromPlainTextContent(string content)
        {
            if (content.Trim() == string.Empty)
            {
                return Resources.GetString(Resource.String.emptyNoteTitle);
            }

            var firstLine = content.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries)[0];

            return new string(firstLine.Take(TitleMaxLength).ToArray());
        }

        /// <summary>
        /// Извлекае заголовок из метаданных HTML-заметки.
        /// </summary>
        /// <returns>Название заметки из метаданных.</returns>
        /// <param name="content">"Сырое" содержимое файла заметки.</param>
        public static string FromMarkupContent(string content)
        {
            var note = new Note(content);
            return note.Metadata.Title;
        }

        /// <summary>
        /// Восстанавливает название заметки из имени ее файла.
        /// </summary>
        /// <returns>Восстановленное название заметки.</returns>
        /// <param name="filename">Имя файла без расширения.</param>
        public static string FromFilename(string filename)
        {
            var chunks = filename.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            var result = string.Empty;

            foreach (var chunk in chunks)
            {
                result += chunk + " ";
            }

            return result.TrimEnd();
        }
    }
}
