﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content;
using System;
using System.IO;

namespace NotesApp
{
    [Activity(Label = "NotesApp", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private const int NoteDeleteConfirmDialog = 1;
        private const int FolderDeleteConfirmDialog = 2;
        
        private AndroidDiskManager _diskManager;
        private ListView _listView;
        private NotesAdapter _dataAdapter;
        private CreateFolderDialog _createFolderDialog;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            NoteTitleHelper.Resources = Resources;

            ActionBar.SetTitle(Resource.String.app_name);

            _diskManager = AndroidDiskManager.GetDefaultInstance(this);
            _listView = FindViewById<ListView>(Resource.Id.listView);

            var folderPopupMenuProvider = new FolderPopupMenuProvider(this, _diskManager);
            var notePopupMenuProvider = new NotePopupMenuProvider(this, _diskManager);
            _dataAdapter = new NotesAdapter(
                this,
                _diskManager,
                folderPopupMenuProvider,
                notePopupMenuProvider);
            _dataAdapter.ListItemClicked += ListItemClickedHandler;

            _listView.Adapter = _dataAdapter;

            _createFolderDialog = new CreateFolderDialog();
            _createFolderDialog.NameSelected += CreateFolderHandler;
        }

        protected override void OnResume()
        {
            //System.Diagnostics.Debug.WriteLine("OnResume Main activity");

            _diskManager.Update();
            _dataAdapter.Update();

            base.OnResume();
        }

		public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.FoldersActivityActionBarMenu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.foldersMenuItemCreateFolder:

                    _createFolderDialog.Show(FragmentManager, "");
                    break;

                case Resource.Id.foldersMenuItemCreateNote:

                    var intent = new Intent(this, typeof(NoteActivity));
                    intent.PutExtra("action", "create");

                    StartActivity(intent);
                    break;

                case Android.Resource.Id.Home:
                    _diskManager.ChangeFolderUp();
                    ActionBar.Subtitle =
                                 Path.GetDirectoryName(ActionBar.Subtitle);

                    if (_diskManager.IsInBaseFolder)
                    {
                        ActionBar.SetDisplayHomeAsUpEnabled(false);
                        ActionBar.Subtitle = string.Empty;
                    }
                    break;

                default:
                    ShowToast("Not supported");
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode != Result.Ok)
            {
                return;
            }

            switch (requestCode)
            {
                case 1:
                    var noteFullFilename = data.GetStringExtra("note");
                    var destPath = data.GetStringExtra("folder");

                    if (!_diskManager.MoveNote(noteFullFilename, destPath))
                    {
                        ShowToast(Resource.String.moveNoteError);
                        return;
                    }

                    ShowToast(Resource.String.moveNoteSuccess);
                    break;

                default:
                    break;
            }
        }

        private void ListItemClickedHandler(object sender, EventArgs<IListViewItem> e)
        {
            if (e.Data is NoteFolder folder)
            {
                _diskManager.ChangeFolder(folder);
                ActionBar.SetDisplayHomeAsUpEnabled(true);
                ActionBar.Subtitle = Path.Combine(
                    ActionBar.Subtitle ?? "/",
                    folder.Name);
            }
            else if (e.Data is NoteFile file)
            {
                var intent = new Intent(this, typeof(NoteActivity));
                intent.PutExtra("action", "open");
                intent.PutExtra("notename", file.FullName);

                StartActivity(intent);
            }
        }

        private void CreateFolderHandler(object sender, EventArgs e)
        {
            var folderName = _createFolderDialog.Name;
            if (!_diskManager.CreateFolder(folderName))
            {
                ShowToast(Resource.String.createFolderError);
                return;
            }

            _dataAdapter.NotifyDataSetChanged();

            var msg = Resources.GetString(Resource.String.folderCreatedText);
            ShowToast(string.Format(msg, folderName));
        }

        private void ShowToast(string text)
        {
            Toast.MakeText(this, text, ToastLength.Long).Show();
        }

        private void ShowToast(int resId)
        {
            Toast.MakeText(this, resId, ToastLength.Long).Show();
        }
    }
}
