﻿using Android.App;
using System;

namespace NotesApp
{
    /// <summary>
    /// Диалог с полем ввода названия.
    /// </summary>
    internal class NameDialogBase : DialogFragment
    {
        /// <summary>
        /// Возникает, когда пользователь нажимает "позитивную" кнопку диалога.
        /// </summary>
        public event EventHandler NameSelected;

        /// <summary>
        /// Введенное название.
        /// </summary>
        public string Name { get; protected set; }

        protected virtual void OnNameSelected()
        {
            NameSelected?.Invoke(this, EventArgs.Empty);
        }
    }
}
