﻿using System.Collections.Generic;

namespace NotesApp
{
    /// <summary>
    /// Метаданные заметки.
    /// </summary>
    internal class NoteMetadata
    {
        /// <summary>
        /// Возвращает или устанавливает заголовок заметки.
        /// </summary>
        public string Title { get; set; } = string.Empty;

        //public List<string> Keywords { get; }

        //public NoteMetadata()
        //{
        //    Keywords = new List<string>();
        //}
    }
}
