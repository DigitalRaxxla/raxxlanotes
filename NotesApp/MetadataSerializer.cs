﻿using System;
using System.Xml.Linq;

namespace NotesApp
{
    /// <summary>
    /// Сериализатор метаданных заметок.
    /// </summary>
    internal class MetadataSerializer : VersionedXmlSerializer<NoteMetadata>
    {
        /// <inheritdoc />
        public override string Save()
        {
            var root = new XElement("Metadata");
            root.SetAttributeValue("version", "1.0");

            var titleElement = new XElement("Title", Data.Title);
            //var keywordsElement = new XElement("Keywords", String.Join(",", Data.Keywords));

            root.Add(titleElement);
            //root.Add(keywordsElement);

            return root.ToString();
        }

        /// <inheritdoc />
        [Version("1.0")]
        private void LoadV1(XElement root)
        {
            Data.Title = root.Element("Title").Value;

            //var keywords = root.Element("Keywords").Value;
            //Data.Keywords.AddRange(
            //keywords.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}
