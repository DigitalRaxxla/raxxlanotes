﻿using Android.Webkit;
using Java.Interop;

namespace NotesApp
{
    /// <summary>
    /// Реализует интерфейс для взаимодействия с движком Javascript HTML-редактора заметок.
    /// Методы этого класса можно вызывать прямо из скриптов из WebView, хостящего HTML-редактор заметок.
    /// </summary>
    public class JavascriptInterface : Java.Lang.Object
    {
        private NoteActivity _activity;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.JavascriptInterface"/>.
        /// </summary>
        /// <param name="activity">Активити редактора заметок.</param>
        public JavascriptInterface(NoteActivity activity)
        {
            _activity = activity;
        }

        /// <summary>
        /// Обработчик события TextChanged редактора заметок.
        /// </summary>
        /// <param name="text">Plain текст заметки.</param>
        [Export]
        [JavascriptInterface]
        public void TextChanged(string text)
        {
            _activity.RunOnUiThread(() =>
                _activity.NoteTitle = NoteTitleHelper.FromPlainTextContent(text));
        }

        /// <summary>
        /// Сохраняет заметку.
        /// </summary>
        /// <param name="text">Plain текст заметки.</param>
        [Export]
        [JavascriptInterface]
        public void SaveNote(string text)
        {
            _activity.RunOnUiThread(() =>
                _activity.SaveNoteContent(text));
        }
    }
}
