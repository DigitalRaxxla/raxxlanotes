﻿using Android.App;
using Android.Widget;
using Android.Views;
using System;
using Android.Content;

namespace NotesApp
{
    /// <summary>
    /// Провайдер контекстного меню заметки.
    /// </summary>
    internal class NotePopupMenuProvider
    {
        private readonly Activity _activity;
        private readonly AndroidDiskManager _diskManager;

        //private RenameNoteDialog _renameNoteDialog;
        private EventHandler<PopupMenu.MenuItemClickEventArgs> _menuItemClickHandlerWrapper;
        //private NoteFile _fileOld;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.NotePopupMenuProvider"/>.
        /// </summary>
        /// <param name="activity">Активити, который будет отображать меню.</param>
        /// <param name="diskManager">Дисковый менеджер заметок.</param>
        public NotePopupMenuProvider(Activity activity, AndroidDiskManager diskManager)
        {
            _activity = activity;
            _diskManager = diskManager;

            //_renameNoteDialog = new RenameNoteDialog();
            //_renameNoteDialog.NameSelected += RenameNoteHandler;
        }

        /// <summary>
        /// Отображает контекстное меню заметки.
        /// </summary>
        /// <param name="view">View, для которого показывается меню.</param>
        /// <param name="file">Файл заметки, для которой показывается меню.</param>
        public void ShowNoteMenu(View view, NoteFile file)
        {
            var menu = new PopupMenu(_activity, view);
            _menuItemClickHandlerWrapper = (sender, e) => MenuItemClickHandler(e, file);
            menu.MenuItemClick += _menuItemClickHandlerWrapper;
            menu.DismissEvent += DismissEventHandler;
            menu.Inflate(Resource.Menu.NotePopupMenu);
            menu.Show();
        }

        private void MenuItemClickHandler(PopupMenu.MenuItemClickEventArgs e, NoteFile file)
        {
            switch (e.Item.ItemId)
            {
                //case Resource.Id.noteMenuItemRename:
                    //_fileOld = file;
                    //_renameNoteDialog.Show(_activity.FragmentManager, "");
                    //break;

                case Resource.Id.noteMenuItemMove:
                    var intent = new Intent(_activity, typeof(NoteMoveActivity));
                    intent.PutExtra("note", file.FullName);
                    _activity.StartActivityForResult(intent, 1);
                    break;

                case Resource.Id.noteMenuItemDelete:
                    var confirm = new ConfirmationDialogBase(
                        Resource.String.deleteConfirmationTitle,
                        Resource.String.noteDeleteConfirmationText);

                    confirm.Confirmed += (sender, ea) =>
                    {
                        if (!_diskManager.DeleteNote(file.FullName))
                        {
                            Toast.MakeText(
                                _activity,
                                Resource.String.deleteNoteError,
                                ToastLength.Long)
                                 .Show();
                            return;
                        }

                        var msg = _activity.Resources.GetString(Resource.String.noteDeletedText);
                        Toast.MakeText(
                            _activity,
                            string.Format(msg, file.Name),
                            ToastLength.Long)
                            .Show();
                    };

                    confirm.Show(_activity.FragmentManager, "");
                    break;
            }
        }

        private void DismissEventHandler(object sender, PopupMenu.DismissEventArgs e)
        {
            e.Menu.DismissEvent -= DismissEventHandler;
            e.Menu.MenuItemClick -= _menuItemClickHandlerWrapper;
        }

        //private void RenameNoteHandler(object sender, EventArgs e)
        //{
        //    var noteNewTitle = _renameNoteDialog.Name;
        //    var noteContent = _diskManager.OpenNote(_fileOld.FullName);
        //    var newLineIndex = noteContent.IndexOf(
        //        Environment.NewLine,
        //        StringComparison.InvariantCulture);

        //    if (newLineIndex == -1)
        //    {
        //        noteContent = noteNewTitle;
        //    }
        //    else
        //    {
        //        noteContent = noteContent.Remove(0, newLineIndex);
        //        noteContent = noteNewTitle + noteContent;
        //    }

        //    _diskManager.SaveNote(_fileOld.FullName, noteContent);

        //    var newFullFilename = _diskManager.CreateNewNoteFilenameFromTitle(noteNewTitle);
        //    var info = new FileInfo(newFullFilename);
        //    var filename = info.Name.Remove(info.Name.Length - NoteFile.FileExtension.Length);

        //    _diskManager.RenameNote(_fileOld, filename);

        //    var msg = _activity.Resources.GetString(Resource.String.done);
        //    Toast.MakeText(_activity, msg, ToastLength.Long).Show();
        //}
    }
}
