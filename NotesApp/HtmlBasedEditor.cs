﻿using System;
using Android.Webkit;

namespace NotesApp
{
    /// <summary>
    /// Реализует команды обощенного текстового редактора для HTML-редактора.
    /// </summary>
    internal class HtmlBasedEditor : IRichTextEditor
    {
        private WebView _webView;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.HtmlBasedEditor"/>.
        /// </summary>
        /// <param name="webView">WebView редактора.</param>
        public HtmlBasedEditor(WebView webView)
        {
            _webView = webView
                ?? throw new ArgumentNullException(nameof(webView));
        }

        /// <inheritdoc>
        public void CommandBold()
        {
            _webView.LoadUrl("javascript:document.execCommand('bold', false, null)");
        }

        /// <inheritdoc>
        public void CommandHighlight()
        {
            _webView.LoadUrl("javascript:document.execCommand('hiliteColor', false, '#fab')");
        }

        /// <inheritdoc>
        public void CommandItalic()
        {
            _webView.LoadUrl("javascript:document.execCommand('italic', false, null)");
        }

        /// <inheritdoc>
        public void CommandUnderline()
        {
            _webView.LoadUrl("javascript:document.execCommand('underline', false, null)");
        }

        /// <inheritdoc>
        public void CommandTitle()
        {
            _webView.LoadUrl("javascript:document.execCommand('formatBlock', false, 'h2')");
        }

        /// <inheritdoc>
        public void CommandSubtitle()
        {
            _webView.LoadUrl("javascript:document.execCommand('formatBlock', false, 'h3')");
        }

        /// <inheritdoc>
        public void CommandCleanStyles()
        {
            _webView.LoadUrl("javascript:document.execCommand('removeFormat', false, null)");
        }
    }
}
