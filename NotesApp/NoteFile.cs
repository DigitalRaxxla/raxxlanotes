﻿using System;
using System.IO;

namespace NotesApp
{
    /// <summary>
    /// Файл заметки.
    /// </summary>
    public class NoteFile : IListViewItem
    {
        /// <summary>
        /// Расширение файла заметки.
        /// </summary>
        public const string FileExtension = ".html";

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.NoteFile"/>.
        /// </summary>
        /// <param name="info">FileInfo файла заметки.</param>
        public NoteFile(FileInfo info)
        {
            FullName = info.FullName;
            Info = info;
            CreationTime = info.CreationTime;

            var noteText = File.ReadAllText(FullName);
            //Name = NoteTitleHelper.FromFilename(
            //info.Name.Substring(0, info.Name.Length - FileExtension.Length));
            Name = NoteTitleHelper.FromMarkupContent(noteText);
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.NoteFile"/>.
        /// </summary>
        /// <param name="fullFilename">Полное имя файла заметки.</param>
        public NoteFile(string fullFilename)
            : this(new FileInfo(fullFilename))
        {
        }

        /// <summary>
        /// Возвращает название заметки.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Возвращает полное имя файла заметки.
        /// </summary>
        public string FullName { get; }

        /// <summary>
        /// Возвращает FileInfo файла заметки.
        /// </summary>
        public FileInfo Info { get; }

        /// <summary>
        /// Возвращает время создания заметки.
        /// </summary>
        public DateTime CreationTime { get; }
    }
}
