﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Android.Content;

namespace NotesApp
{
    /// <summary>
    /// Реализует функциональность по манипуляции заметками:
    /// создание, перемещение, удаление, чтение, сохранение, переименование.
    /// Работает с заметками и папками.
    /// Так же предоставляет спомогательные функции, типа генерации имени файла заметки.
    /// </summary>
    public class AndroidDiskManager
    {
        private static AndroidDiskManager s_defaultInstance;

        public static AndroidDiskManager GetDefaultInstance(ContextWrapper context)
        {
            if (s_defaultInstance != null)
            {
                return s_defaultInstance;
            }

            var appDirName = context.Resources.GetString(Resource.String.defaultFolder);
            var documentsDir = context.GetExternalFilesDir(Android.OS.Environment.DirectoryDocuments);
            var notesDir = Path.Combine(documentsDir.AbsolutePath, appDirName);

            if (!Directory.Exists(notesDir))
            {
                Directory.CreateDirectory(notesDir);
            }

            s_defaultInstance = new AndroidDiskManager(notesDir);
            return s_defaultInstance;
        }

        private string _baseDir;
        private DirectoryInfo _currentFolder;
        private NoteFolder[] _folders;
        private NoteFile[] _notes;

        /// <summary>
        /// Возникает, когда происходят изменения в файловой системе,
        /// влияющие на отображение списка папок и заметок в приложении.
        /// </summary>
        public event EventHandler ContentUpdated;

        /// <summary>
        /// Базовая директория приложения.
        /// </summary>
        public static string BaseAppDir { get; set; }

        /// <summary>
        /// Возвращает массив папок в текущей директории.
        /// </summary>
        public NoteFolder[] Folders => _folders;

        /// <summary>
        /// Возвращает массив файлов заметок в текущей директории.
        /// </summary>
        public NoteFile[] Notes => _notes;

        /// <summary>
        /// Возвращает текущую директорию.
        /// </summary>
        public NoteFolder CurrentFolder => new NoteFolder(_currentFolder);

        /// <summary>
        /// Находимся ли сейчас в базовой директории данного менеджера.
        /// </summary>
        public bool IsInBaseFolder
        {
            get => _currentFolder.FullName == _baseDir;
        }

        /// <summary>
        /// Находимся ли сейчас в базовой директории приложения.
        /// </summary>
        public bool IsInBaseAppFolder
        {
            get => _currentFolder.FullName == BaseAppDir;
        }

        /// <summary>
        /// Создает новый экземпляр менеджера.
        /// </summary>
        /// <param name="baseDir">Базовая директория для менеджера.</param>
        public AndroidDiskManager(string baseDir)
        {
            _baseDir = baseDir;
            _currentFolder = new DirectoryInfo(baseDir);

            if (BaseAppDir == null)
            {
                BaseAppDir = baseDir;
            }

            Update();
        }

        /// <summary>
        /// Обновляет списки папок и заметок в текущей директории,
        /// которыми оперирует менеджер.
        /// </summary>
        public void Update()
        {
            _folders = _currentFolder.EnumerateDirectories()
                .Select(info => new NoteFolder(info))
                .ToArray();

            _notes = _currentFolder.EnumerateFiles(
                $"*{NoteFile.FileExtension}",
                SearchOption.TopDirectoryOnly)
                    .Select(info => new NoteFile(info))
                    .ToArray();
        }

        /// <summary>
        /// Меняет текущую рабочую директорию.
        /// </summary>
        /// <param name="folder">Новая рабочая директория.</param>
        public void ChangeFolder(NoteFolder folder)
        {
            _currentFolder = folder.Info;
            OnContentUpdated();
        }

        /// <summary>
        /// Переходит вверх по иерархии папок.
        /// </summary>
        public void ChangeFolderUp()
        {
            if (!IsInBaseFolder)
            {
                ChangeFolder(new NoteFolder(_currentFolder.Parent));
            }
        }

        /// <summary>
        /// Создает новую папку в текущей директории.
        /// </summary>
        /// <returns><c>true</c>, если папка была создана, иначе — <c>false</c>.</returns>
        /// <param name="folderName">Имя создаваемой папки.</param>
        public bool CreateFolder(string folderName)
        {
            try
            {
                _currentFolder.CreateSubdirectory(folderName);
                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Переименовывает папку.
        /// </summary>
        /// <returns><c>true</c>, если папка была переименована, иначе — <c>false</c>.</returns>
        /// <param name="folder">Папка, которую надо переименовать.</param>
        /// <param name="newName">Новое имя папки.</param>
        public bool RenameFolder(NoteFolder folder, string newName)
        {
            try
            {
                var parent = folder.Info.Parent;
                folder.Info.MoveTo(Path.Combine(parent.FullName, newName));

                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Удаляет папку.
        /// </summary>
        /// <returns><c>true</c>, если папка была удалена, иначе — <c>false</c>.</returns>
        /// <param name="folder">Удаляемая папка.</param>
        public bool DeleteFolder(NoteFolder folder)
        {
            try
            {
                folder.Info.Delete(true);
                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Создает пустой файл заметки.
        /// </summary>
        /// <returns><c>true</c>, если файл заметки был создан, иначе — <c>false</c>.</returns>
        /// <param name="fullFilename">Полное имя файла заметки.</param>
        public bool CreateNote(string fullFilename)
        {
            try
            {
                File.WriteAllText(
                    fullFilename,
                    string.Empty);
                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Переименовывае файл заметки.
        /// </summary>
        /// <param name="note">Файл заметки, который переименовывается.</param>
        /// <param name="newBaseName">Новое имя файла без расширения.</param>
        /// <returns>Новое имя файла (полный путь).</returns>
        public string RenameNote(NoteFile note, string newBaseName)
        {
            try
            {
                var newFullName = CreateExactNoteFilenameFromTitle(newBaseName);

                if (note.FullName == newFullName)
                {
                    return null;
                }

                note.Info.MoveTo(newFullName);
                OnContentUpdated();

                return newFullName;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Перемещает файл заметки.
        /// </summary>
        /// <returns><c>true</c>, если заметка была перемещена, иначе — <c>false</c>.</returns>
        /// <param name="fullFilename">Полное имя файла заметки.</param>
        /// <param name="newPath">Полный путь к папке, куда перемещается заметка.</param>
        public bool MoveNote(string fullFilename, string newPath)
        {
            try
            {
                var info = new FileInfo(fullFilename);
                info.MoveTo(Path.Combine(newPath, info.Name));
                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Считывает заметку.
        /// </summary>
        /// <returns>Содержимое файла заметки.</returns>
        /// <param name="fullFilename">Полное имя файла заметки.</param>
        public string OpenNote(string fullFilename)
        {
            return File.ReadAllText(fullFilename, Encoding.UTF8);
        }

        /// <summary>
        /// Сохраняет содержимое заметки.
        /// </summary>
        /// <returns><c>true</c>, если заметка была сохранена, иначе — <c>false</c>.</returns>
        /// <param name="fullFilename">Полное имя файла заметки.</param>
        /// <param name="content">Сохраняемое в файл содержимое.</param>
        public bool SaveNote(string fullFilename, string content)
        {
            try
            {
                File.WriteAllText(
                    fullFilename,
                    content,
                    Encoding.UTF8);

                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Удаляет файл заметки.
        /// </summary>
        /// <returns><c>true</c>, если заметка была удалена, иначе — <c>false</c>.</returns>
        /// <param name="fullFilename">Полное имя файла заметки.</param>
        public bool DeleteNote(string fullFilename)
        {
            try
            {
                File.Delete(fullFilename);
                OnContentUpdated();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Формирует полное имя файла заметки из "базового имени" (без пути и расширения).
        /// </summary>
        /// <returns>Полное имя файла заметки.</returns>
        /// <param name="baseName">"Базовое имя" заметки — имя файла без расширения.</param>
        /// <remarks>В качестве пути к файлу используется путь к текущей папке.</remarks>
        public string GetNoteFilename(string baseName)
        {
            return Path.Combine(
                _currentFolder.FullName,
                baseName + NoteFile.FileExtension);
        }

        /// <summary>
        /// Формирует новое, безопасное для текущей папки, полное имя файла замети из текста ее заголовка.
        /// Возникающие коллизии с существующими файлами разрешаются.
        /// </summary>
        /// <returns>Полное имя файла заметки.</returns>
        /// <param name="title">Заголовок заметки.</param>
        /// <remarks>
        /// При формировании имени файла "опасные" символы (в т.ч. пробелы) заменяются на подчеркивание.
        /// </remarks>
        public string CreateNewNoteFilenameFromTitle(string title)
        {
            var baseName = EscapeTitle(title);
            var filename = GetNoteFilename(baseName);
            var i = 1;

            while (File.Exists(filename))
            {
                var newBaseName = $"{baseName}_{i}";
                filename = GetNoteFilename(newBaseName);
                i++;
            }

            return filename;
        }

        /// <summary>
        /// Формирует полное имя файла заметки из текста ее заголовка
        /// без разрешения коллизий с существующими файлами.
        /// </summary>
        /// <returns>Полное имя файла заметки.</returns>
        /// <param name="title">Заголовок заметки.</param>
        /// <remarks>
        /// При формировании имени файла "опасные" символы (в т.ч. пробелы) заменяются на подчеркивание.
        /// </remarks>
        public string CreateExactNoteFilenameFromTitle(string title)
        {
            var baseName = EscapeTitle(title);
            return GetNoteFilename(baseName);
        }

        protected virtual void OnContentUpdated()
        {
            Update();
            ContentUpdated?.Invoke(this, EventArgs.Empty);
        }

        private string GetFullName(string folderName)
        {
            return Path.Combine(_baseDir, folderName);
        }

        private string EscapeTitle(string title)
        {
            var invalidChars = new[] { ' ', ':', '?', '/', '\\', '<', '>', '"', '\'', '|', '*', ',' };
            var escaped = title;

            foreach (var ch in invalidChars)
            {
                escaped = escaped.Replace(ch, '_');
            }

            return escaped;
        }
    }
}
