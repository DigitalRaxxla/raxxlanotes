﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Android.Graphics;
using System.Threading;

namespace NotesApp
{
    [Activity(Label = "Note")]
    public class NoteActivity : Activity
    {
        private readonly AutoResetEvent _saveEvent = new AutoResetEvent(false);

        private WebView _webView;
        //private Button _contentButton;
        private Button _boldButton;
        private Button _italicButton;
        private Button _underlineButton;
        private Button _titleButton;
        private Button _subtitleButton;
        private AndroidDiskManager _diskManager;
        private string _noteFullFilename;
        private bool _deleting;
        private IRichTextEditor _editor;

        /// <summary>
        /// Возвращает или устанавливает заголовок заметки.
        /// При установке заголовка, перрееименовывается файл заметки.
        /// </summary>
        public string NoteTitle
        {
            get => ActionBar.Title;
            set
            {
                ActionBar.Title = value;

                var newFilename = _diskManager.RenameNote(
                    new NoteFile(_noteFullFilename),
                    value);

                _noteFullFilename = newFilename ?? _noteFullFilename;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.NoteActivityActionBarMenu, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menuItemMove:
                    var intent = new Intent(this, typeof(NoteMoveActivity));
                    intent.PutExtra("note", _noteFullFilename);
                    StartActivityForResult(intent, 1);
                    break;

                case Resource.Id.menuItemDelete:
                    if (!_diskManager.DeleteNote(_noteFullFilename))
                    {
                        Toast.MakeText(
                            this,
                            Resource.String.deleteNoteError,
                            ToastLength.Long)
                             .Show();
                        break;
                    }

                    _deleting = true;
                    Finish();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        public override bool OnNavigateUp()
        {
            Finish();
            return true;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.Note);

            var underlineButton = FindViewById<Button>(Resource.Id.underlineButton);
            underlineButton.PaintFlags |= PaintFlags.UnderlineText;

            _webView = FindViewById<WebView>(Resource.Id.webView);
            _webView.SetWebChromeClient(new WebChromeClient()); 
            _webView.Settings.JavaScriptEnabled = true;
            _webView.Settings.DomStorageEnabled = true;

            _editor = new HtmlBasedEditor(_webView);

            //_contentButton = FindViewById<Button>(Resource.Id.contentButton);
            _boldButton = FindViewById<Button>(Resource.Id.boldButton);
            _italicButton = FindViewById<Button>(Resource.Id.italicButton);
            _underlineButton = FindViewById<Button>(Resource.Id.underlineButton);
            _titleButton = FindViewById<Button>(Resource.Id.titleButton);
            _subtitleButton = FindViewById<Button>(Resource.Id.headingButton);

            //_contentButton.Click += (s, e) => _webView.LoadUrl("javascript:saveNote()");
            _boldButton.Click += (s, e) => _editor.CommandBold();
            _italicButton.Click += (s, e) => _editor.CommandItalic();
            _underlineButton.Click += (s, e) => _editor.CommandUnderline();
            _titleButton.Click += (s, e) => _editor.CommandTitle();
            _subtitleButton.Click += (s, e) => _editor.CommandSubtitle();

            ActionBar.SetDisplayHomeAsUpEnabled(true);

            _diskManager = AndroidDiskManager.GetDefaultInstance(this);

            var title = string.Empty;
            var content = EmbeddedResource.GetString("HtmlTemplate.html");

            var action = Intent.GetStringExtra("action");
            switch (action)
            {
                case "create":
                    title = NoteTitleHelper.CreateDefault();
                    _noteFullFilename = _diskManager.CreateNewNoteFilenameFromTitle(title);
                    if (!_diskManager.CreateNote(_noteFullFilename))
                    {
                        Toast.MakeText(
                            this,
                            Resource.String.createNoteError,
                            ToastLength.Long)
                             .Show();
                        return;
                    }

                    content = content.Replace("{content}", $"<h2>{title}</h2>");

                    LoadNoteContent(content);
                    break;

                case "open":
                    _noteFullFilename = Intent.GetStringExtra("notename");
                    var noteContent = _diskManager.OpenNote(_noteFullFilename);

                    content = content.Replace("{content}", noteContent);

                    LoadNoteContent(content);
                    break;

                default:
                    throw new NotSupportedException($"Action: {action}");
            }

            SetSubtitle();
        }

        protected override void OnPause()
        {
            if (!_deleting)
            {
                SaveNote();
            }

            base.OnPause();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (resultCode != Result.Ok)
            {
                return;
            }

            switch (requestCode)
            {
                case 1:
                    var destPath = data.GetStringExtra("folder");

                    if (!_diskManager.MoveNote(_noteFullFilename, destPath))
                    {
                        Toast.MakeText(
                            this,
                            Resource.String.moveNoteError,
                            ToastLength.Long).Show();
                        return;
                    }

                    _diskManager.ChangeFolder(
                        new NoteFolder(
                            new DirectoryInfo(destPath)));
                    var info = new FileInfo(_noteFullFilename);
                    _noteFullFilename = System.IO.Path.Combine(destPath, info.Name);
                    SetSubtitle();
                    break;

                default:
                    break;
            }
        }

        internal void SaveNoteContent(string content)
        {
            var note = new Note(content);
            note.Metadata.Title = NoteTitle;

            var noteString = note.ToString();

			System.Diagnostics.Debug.WriteLine($"Saving note [{_noteFullFilename}].");

            if (!_diskManager.SaveNote(_noteFullFilename, noteString))
            {
                Toast.MakeText(
                    this,
                    Resource.String.saveNoteError,
                    ToastLength.Long)
                     .Show();
            }
        }

        private void LoadNoteContent(string html)
        {
            _webView.LoadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
            _webView.AddJavascriptInterface(new JavascriptInterface(this), "Android");
        }

        private void SaveNote()
        {
			System.Diagnostics.Debug.WriteLine("Calling javascript to save note.");

            _webView.LoadUrl("javascript:saveNote()");
        }

        private void SetSubtitle()
        {
            var dirName = _diskManager.IsInBaseAppFolder
                ? string.Empty
                : _diskManager.CurrentFolder.Name;
            
            ActionBar.Subtitle = $"/{dirName}";
        }
    }
}
