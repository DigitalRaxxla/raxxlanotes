﻿using Android.App;
using Android.OS;
using Android.Widget;

namespace NotesApp
{
    /// <summary>
    /// Диалог переименования папки.
    /// </summary>
    internal class RenameFolderDialog : NameDialogBase
    {
        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var builder = new AlertDialog.Builder(Activity);
            var inflater = Activity.LayoutInflater;

            builder.SetTitle(Resource.String.renameFolderTitle);
            builder.SetView(inflater.Inflate(Resource.Layout.FolderNameDialog, null));
            builder.SetPositiveButton(
                Resource.String.menuItemRenameTitle,
                (sender, e) =>
                {
                    var edit = Dialog.FindViewById<EditText>(Resource.Id.folderName);

                    Name = edit.Text;
                    OnNameSelected();
                });
            builder.SetNegativeButton(Resource.String.cancel, (sender, e) => Dialog.Cancel());

            return builder.Create();
        }
    }
}
