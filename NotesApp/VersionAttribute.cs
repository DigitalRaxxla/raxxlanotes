﻿using System;

namespace NotesApp
{
    /// <summary>
    /// Атрибут, указывающий версию.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    internal class VersionAttribute : Attribute
    {
        /// <summary>
        /// Возвращает или устанавливает значение версии.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.VersionAttribute"/>.
        /// </summary>
        /// <param name="version">Значение версии.</param>
        public VersionAttribute(string version)
        {
            Version = version;
        }
    }
}
