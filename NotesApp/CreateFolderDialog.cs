﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace NotesApp
{
    internal class CreateFolderDialog : NameDialogBase
    {
        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var builder = new AlertDialog.Builder(Activity);
            var inflater = Activity.LayoutInflater;

            builder.SetTitle(Resource.String.createFolderTitle);
            builder.SetView(inflater.Inflate(Resource.Layout.FolderNameDialog, null));
            builder.SetPositiveButton(
                Resource.String.menuItemCreateFolderTitle,
                (sender, e) =>
                {
                    var edit = Dialog.FindViewById<EditText>(Resource.Id.folderName);
                    
                    Name = edit.Text;
                    OnNameSelected();
                });
            builder.SetNegativeButton(Resource.String.cancel, (sender, e) => Dialog.Cancel());

            return builder.Create();
        }
    }
}
