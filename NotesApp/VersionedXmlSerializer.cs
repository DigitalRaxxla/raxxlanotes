﻿using System.Xml.Linq;
using System.Reflection;
using System.Linq;
using System.Xml;

namespace NotesApp
{
    /// <summary>
    /// Абстрактный класс-сериализатор версионированных xml.
    /// </summary>
    /// <remarks>
    /// Позволяет реализовать загрузку xml-данных разных версий и сохранение текущей актуальной версии.
    /// Версия прописывается в атрибуте <c>version</c> корневого тега.
    /// Методы загрузки реализуются в классах-наследниках. Должны быть приватными.
    /// Соответствие конкретного метода конкретной версии задается атрибутом <c>Version</c> у метода.
    /// </remarks>
    internal abstract class VersionedXmlSerializer<T>
        where T : new()
    {
        /// <summary>
        /// Возвращает модель данных, хранимых в XML.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.VersionedXmlSerializer`1"/>.
        /// </summary>
        public VersionedXmlSerializer()
        {
            Data = new T();
        }

        /// <summary>
        /// Сериализует данные из <c>Data</c> в XML.
        /// </summary>
        /// <returns>Строка с XML.</returns>
        public abstract string Save();

        /// <summary>
        /// Десериализует XML в модель данных <c>Data</c>.
        /// </summary>
        /// <param name="xml">Строка с XML.</param>
        public void Load(string xml)
        {
            var xdoc = XDocument.Parse(xml);
            var root = xdoc.Root;
            var versionElement = root.Attribute("version");

            if (versionElement == null)
            {
                throw new XmlException("Version attribute is absent.");
            }

            var version = versionElement.Value;

            var type = GetType();
            var methods = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);
            var loadMethod = methods.First(m =>
            {
                var attr = (VersionAttribute)
                    m.GetCustomAttributes(typeof(VersionAttribute), false).First();
                return attr.Version == version;
            });

            loadMethod.Invoke(this, new[] { root });
        }
    }
}
