﻿using Android.App;
using Android.Widget;
using Android.Views;
using System;

namespace NotesApp
{
    /// <summary>
    /// Адаптер данных для списка элементов "проводника" заметок.
    /// </summary>
    internal class NotesAdapter : BaseAdapter
    {
        private readonly Activity _activity;
        private readonly AndroidDiskManager _diskManager;
        private readonly LayoutInflater _layoutInflater;
        private readonly FolderPopupMenuProvider _folderPopupMenu;
        private readonly NotePopupMenuProvider _notePopupMenu;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:NotesApp.NotesAdapter"/>.
        /// </summary>
        /// <param name="activity">Активити, содержащий список.</param>
        /// <param name="diskManager">Дисковый менеджер заметок.</param>
        /// <param name="folderPopupMenu">Провайдер контекстного меню папок.</param>
        /// <param name="notePopupMenu">Провайдер контекстного меню заметок.</param>
        public NotesAdapter(
            Activity activity,
            AndroidDiskManager diskManager,
            FolderPopupMenuProvider folderPopupMenu,
            NotePopupMenuProvider notePopupMenu)
        {
            _activity = activity;
            _diskManager = diskManager;
            _layoutInflater = activity.LayoutInflater;
            _folderPopupMenu = folderPopupMenu;
            _notePopupMenu = notePopupMenu;

            _diskManager.ContentUpdated += (sender, e) => Update();
        }

        /// <summary>
        /// Возникает, когда пользователь "тапает" элемент списка.
        /// </summary>
        public event EventHandler<EventArgs<IListViewItem>> ListItemClicked;

        /// <summary>
        /// Возвращает количество элементов списка (папки + заметки).
        /// </summary>
        public override int Count => _diskManager.Folders.Length + _diskManager.Notes.Length;

        /// <summary>
        /// Обновляет список элементов (заново считывает данные из файловой системы).
        /// </summary>
        public void Update()
        {
            _activity.RunOnUiThread(() => NotifyDataSetChanged());
        }

        /// <inheritdoc />
        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        /// <inheritdoc />
        public override long GetItemId(int position)
        {
            return position;
        }

        /// <inheritdoc />
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var listItemView = _layoutInflater.Inflate(Resource.Layout.ListItemWithMenu, parent, false);
            var item = position < _diskManager.Folders.Length
                ? (IListViewItem)_diskManager.Folders[position]
                : _diskManager.Notes[position - _diskManager.Folders.Length];

            var title = listItemView.FindViewById<TextView>(Resource.Id.title);
            title.SetText(item.Name, TextView.BufferType.Normal);

            var date = listItemView.FindViewById<TextView>(Resource.Id.subTitle);
            date.SetText(
                item.CreationTime.ToShortDateString(),
                TextView.BufferType.Normal);

            var menuButton = listItemView.FindViewById<Button>(Resource.Id.contextMenuButton);
            var image = listItemView.FindViewById<ImageView>(Resource.Id.icon);
            if (item is NoteFolder folder)
            {
                menuButton.Click += (sender, e) => _folderPopupMenu.ShowFolderMenu(listItemView, folder);
                image.SetImageResource(Resource.Drawable.ic_folder_white_24dp);
            }
            else if (item is NoteFile file)
            {
                menuButton.Click += (sender, e) => _notePopupMenu.ShowNoteMenu(listItemView, file);
                image.SetImageResource(Resource.Drawable.ic_insert_drive_file_white_24dp);
            }

            _activity.RegisterForContextMenu(listItemView);
            listItemView.Click += (s, e) => ListItemClickHandler(item);

            return listItemView;
        }

        protected virtual void ListItemClickHandler(IListViewItem item)
        {
            ListItemClicked?.Invoke(this, new EventArgs<IListViewItem>(item));
        }
    }
}
