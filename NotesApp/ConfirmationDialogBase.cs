﻿using System;
using Android.App;
using Android.OS;

namespace NotesApp
{
    /// <summary>
    /// Диалог с текстом и кнопками "Да", "Нет".
    /// </summary>
    internal class ConfirmationDialogBase : DialogFragment
    {
        protected readonly int _titleId;
        protected readonly int _messageId;

        /// <summary>
        /// Возникает, если пользователь нажимает кнопку "Да".
        /// </summary>
        public event EventHandler Confirmed;

        /// <summary>
        /// Инициализирует новый экзепляр класса <see cref="T:NotesApp.ConfirmationDialogBase"/>.
        /// </summary>
        /// <param name="titleId">Идентификатор текста заголовка.</param>
        /// <param name="messageId">Идентификатор текста сообщения.</param>
        public ConfirmationDialogBase(int titleId, int messageId)
        {
            _titleId = titleId;
            _messageId = messageId;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState)
        {
            var builder = new AlertDialog.Builder(Activity);
            var inflater = Activity.LayoutInflater;

            builder.SetTitle(_titleId);
            builder.SetMessage(_messageId);
            builder.SetPositiveButton(Resource.String.yes, (sender, e) => OnConfirmed());
            builder.SetNegativeButton(Resource.String.no, (sender, e) => Dialog.Cancel());

            return builder.Create();
        }

        protected virtual void OnConfirmed()
        {
            Confirmed?.Invoke(this, EventArgs.Empty);
        }
    }
}
