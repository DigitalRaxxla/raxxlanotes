﻿using System;

namespace NotesApp
{
    /// <summary>
    /// Обощенный интерфейс элемента списка "проводника" заметок.
    /// </summary>
    public interface IListViewItem
    {
        /// <summary>
        /// Возвращает имя элемента.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Возвращает дату создания элемента.
        /// </summary>
        DateTime CreationTime { get; }
    }
}
