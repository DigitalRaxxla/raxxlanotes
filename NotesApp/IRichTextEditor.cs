﻿namespace NotesApp
{
    /// <summary>
    /// Интерфейс текстового редактора.
    /// </summary>
    internal interface IRichTextEditor
    {
        /// <summary>
        /// Делает текст жирным.
        /// </summary>
        void CommandBold();

        /// <summary>
        /// Делает текст курсивным.
        /// </summary>
        void CommandItalic();

        /// <summary>
        /// Делает текст подчеркнутым.
        /// </summary>
        void CommandUnderline();

        /// <summary>
        /// Выделяет фон текста.
        /// </summary>
        void CommandHighlight();

        /// <summary>
        /// Делает текст заголовком.
        /// </summary>
        void CommandTitle();

        /// <summary>
        /// Делает текст подзаголовком.
        /// </summary>
        void CommandSubtitle();

        /// <summary>
        /// Очищает стили текста.
        /// </summary>
        void CommandCleanStyles();
    }
}
